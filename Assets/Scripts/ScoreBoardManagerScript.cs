﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreBoardManagerScript : MonoBehaviour {

	// Number of score slots to hold
	public int numberOfSlots = 10;

	// How fast to animate text
	public float Delay = 0.1f;

	// High score message delay
	public float highScoreDelayInSeconds = 1.0f;

	// High score message move speed
	public float highScoreTextMoveSpeed = 0.1f;

	// Current scores
	List<LeaderboardEntry> Scores;

	// All prefabs loaded
	List<GameObject> scoreBoardObjects;

	// HighScore Prefab
	GameObject hsPrefabInstance;

	// Highscore has finished animating
	bool highScoreAnimated = false;

	// Use this for initialization
	void Start () {
		scoreBoardObjects = new List<GameObject>();
		SetupPrefabs ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Load all prefabs into the scene
	void SetupPrefabs() {
		// Create score objects based on slots
		for (var i = 0; i < numberOfSlots; i++) {
			// Load a prefab
			var res = Resources.Load ("ScoreBoardObject");

			// Set the position
			Vector3 prefabLocation = Camera.main.transform.position + (Camera.main.transform.forward * 10);
			prefabLocation.x += 1.1f;
			prefabLocation.y -= (i * 0.68f) - 2.5f;

			// Instantiate it in front of the camera
			GameObject prefabInstance = Instantiate (res, prefabLocation, Camera.main.transform.rotation) as GameObject;

			// Empty it's text
			prefabInstance.GetComponent<ScoreBoardObjectManagerScript> ().SetText ("", "", Delay, false);

			// Add it to the list
			scoreBoardObjects.Add(prefabInstance);
		}

		// Create a highscore prefab
		var hsRes = Resources.Load("HighScoreObject");

		// Set the position
		Vector3 hsPrefabLocation = Camera.main.transform.position + (Camera.main.transform.forward * 5);
		hsPrefabLocation.y += 0;

		// Instantiate it
		hsPrefabInstance = Instantiate (hsRes, hsPrefabLocation, Camera.main.transform.rotation) as GameObject;

		// Deactivate it and use it for later
		hsPrefabInstance.SetActive (false);
	}

	// Set the new current display scores
	public void SetNewScores(List<LeaderboardEntry> newScores, List<int> changedIndeces) {
		// Set the new scores
		List<LeaderboardEntry> oldScores = Scores;
		Scores = newScores;

		// Apply the scores incrementally
		StartCoroutine (applyScoresIncremental (changedIndeces, oldScores));
	}

	// Trickle down top to bottom score animating
	IEnumerator applyScoresIncremental(List<int> changedIndeces, List<LeaderboardEntry> oldScores) {
		// Reset the the highscore animated field
		highScoreAnimated = false;

		// Set the texts
		for (var i = 0; i < scoreBoardObjects.Count; i++) {
			if (changedIndeces.Contains (i)) {

				// Set the new text
				string newText = Scores [i].name;
				string scoreText = Scores [i].value.ToString();

				// New high score
				if (i == 0) {
					// Activate the high score dingy
					hsPrefabInstance.SetActive (true);

					// Begin the animation for the high score display
					hsPrefabInstance.GetComponent<HighScoreObjectManagerScript> ().BeginAnimate (newText, scoreText, scoreBoardObjects[i].transform.position, highScoreDelayInSeconds, highScoreTextMoveSpeed, hasFinished);

					// Wait until the text animation begins
					yield return new WaitForSeconds (highScoreDelayInSeconds);

					// Set the current top score to be empty as soon as the high score text starts moving
					scoreBoardObjects [i].GetComponent<ScoreBoardObjectManagerScript> ().SetText ("", "", Delay, true);

					// Wait until the high score text finishes moving
					yield return new WaitUntil(() => highScoreAnimated);

					// Set the high score to be the new value
					scoreBoardObjects [i].GetComponent<ScoreBoardObjectManagerScript> ().SetText (newText, scoreText, 0.0f, true);
				} else {
					// Set the new score text
					if ((Scores != null) && (oldScores != null)) {
						bool animate = true;
						for (var y=0; y<oldScores.Count; y++) {
							//for (var w=0; w<Scores.Count; w++) {
								if (oldScores [y].id == Scores [i].id) {
									animate = false;
								}
							//}
						}
						scoreBoardObjects [i].GetComponent<ScoreBoardObjectManagerScript> ().SetText (newText, scoreText, Delay, animate);
					} else {
						scoreBoardObjects [i].GetComponent<ScoreBoardObjectManagerScript> ().SetText (newText, scoreText, Delay, true);
					}

					// Wait for a moment to create the trickle down effect
					yield return new WaitForSeconds (0.1f);
				}
			}
		}
	}

	// Callback for high score animation
	public void hasFinished(string newText) {
		// Allow the script to continue
		highScoreAnimated = true;

		// Set the high score to be the new value
		scoreBoardObjects [0].GetComponent<ScoreBoardObjectManagerScript> ().SetText (newText, "", 0.0f, true);
	}
}

