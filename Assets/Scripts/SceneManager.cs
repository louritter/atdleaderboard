﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour {

	// Setup states
	enum States {
		START,
		IDLE,
		NEWHIGHSCORE,
		NEWSCORE,
		COMBINED
	}

	// We begin in the start state
	private States state = States.START;

	// Set up list to store all changed score index positions
	List<int> changedIndeces = new List<int> ();

	// Set up local copy of scores
	List<LeaderboardEntry> scores;

	// Script references
	ScoresServiceScript ScoresService;
	ScoreBoardManagerScript ScoreBoardManager;

	// Use this for initialization
	void Start () {
		// Run in background
		Application.runInBackground = true;

		// Get script references
		ScoresService = GameObject.FindObjectOfType (typeof(ScoresServiceScript)) as ScoresServiceScript;
		ScoreBoardManager = GameObject.FindObjectOfType (typeof(ScoreBoardManagerScript)) as ScoreBoardManagerScript;

		// Have the ScoresService score polling
		ScoresService.beginPolling ();

		// We move to the idle state after starting up
		MoveToState(States.IDLE);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Transition the current state
	void MoveToState(States newState) {
		switch (newState) {
		case States.START: 
			throw new System.NotSupportedException ("Moving to START is not supported.");
		case States.IDLE:
			HandleIdleState ();
			break;
		case States.NEWHIGHSCORE:
			HandleNewHighscoreState ();
			break;
		case States.NEWSCORE:
			HandleNewScoreState ();
			break;
		case States.COMBINED:
			HandleCombinedState ();
			break;
		}
	}

	// Handle idle state
	void HandleIdleState() {
		// Set state
		if (state == States.IDLE) {
			throw new System.NotSupportedException ("Can't move to IDLE state from IDLE state");
		}
		state = States.IDLE;
	}

	// Handle new high score state
	void HandleNewHighscoreState() {
		// Set state
		if (state != States.IDLE) {
			throw new System.NotSupportedException ("Can only move to NEWHIGHSCORE state from IDLE state");
		}
		state = States.NEWHIGHSCORE;

		// Send new score
		ScoreBoardManager.SetNewScores(scores, changedIndeces);

		state = States.IDLE;
	}

	// Handle new score state
	void HandleNewScoreState() {
		// Set state
		if (state != States.IDLE) {
			throw new System.NotSupportedException ("Can only move to NEWSCORE state from IDLE state");
		}
		state = States.NEWSCORE;

		// Send new score
		ScoreBoardManager.SetNewScores(scores, changedIndeces);

		state = States.IDLE;
	}

	// Handle combined state
	void HandleCombinedState() {
		// Set state
		if (state != States.IDLE) {
			throw new System.NotSupportedException ("Can only move to COMBINED state from IDLE state");
		}
		state = States.COMBINED;

		// Send new score
		ScoreBoardManager.SetNewScores(scores, changedIndeces);

		state = States.IDLE;
	}

	// Handle any changes to the score
	public void HandleNewScores(List<LeaderboardEntry> _new, List<LeaderboardEntry> _old) {
		// Set local copy of scores
		scores = _new;

		// Clear changed indeces
		changedIndeces.Clear();

		// Loop through new data
		for (var i = 0; i < _new.Count; i++) {
			if (i >= _old.Count) {
				changedIndeces.Add(i);
			} else if ((string.Compare(_new[i].name, _old[i].name) != 0) || (_new[i].value != _old[i].value)) {
				changedIndeces.Add(i);
			}
		}

		// Determine type of change
		if (changedIndeces.Count != 0) {
			if (changedIndeces [0] == 0) {
				if (changedIndeces.Count > 1) {
					//Combined
					MoveToState(States.COMBINED);
				} else {
					//New High Score
					MoveToState(States.NEWHIGHSCORE);
				}
			} else {
				//New Score
				MoveToState(States.NEWSCORE);
			}
		}
	}
}
