﻿using System;
using UnityEngine;

[System.Serializable]
public class LeaderboardEntry
{
	public string name;
	public int value;
	public int id;
}