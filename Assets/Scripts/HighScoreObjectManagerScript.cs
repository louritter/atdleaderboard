﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreObjectManagerScript : MonoBehaviour {

	// Hold a reference to our textmesh
	GameObject text;

	// Starting text position
	Vector3 a;

	// The animation finished callback
	System.Action<string> callback;

	// Next text
	string nextScore;

	// Scale down trigger
	bool scaleDown;

	// Use this for initialization
	void Awake () {
		// Set the position of where text should go
		a = gameObject.transform.position + new Vector3 (0.6f, -0.4f, -1);
	}

	// Begin to animate the display
	public void BeginAnimate(string name, string score, Vector3 newPos, float animateDelay, float moveSpeed, System.Action<string> cb) {
		// Set the callback
		callback = cb;

		var res = Resources.Load ("ScoreBoardObject");

		// Duplicate high score TMP
		GameObject tmpText = Instantiate (res, a, gameObject.transform.rotation) as GameObject;
		tmpText.GetComponent<ScoreBoardObjectManagerScript> ().SetText (name, score, 0.0f, true);
		tmpText.transform.SetParent(gameObject.transform);

		// Save the reference
		text = tmpText;

		// Set the reference
		nextScore = name;

		// Get the animator
		Animator anim = gameObject.GetComponent<Animator> ();

		// Begin grow animation
		StartCoroutine (AnimateObject (anim, newPos, animateDelay, moveSpeed));
	}

	IEnumerator AnimateObject(Animator animator, Vector3 newPos, float animateDelay, float moveSpeed) {
		// Begin the flipping animation
		animator.SetTrigger ("BeginGrow");

		// Wait for animation to be done
		yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo (0).IsName ("Idle"));

		// Begin moving the text to correct place
		StartCoroutine (TranslateText (newPos, animateDelay, animator, moveSpeed));
	}

	// Animate the transition
	IEnumerator TranslateText(Vector3 newPos, float animateDelay, Animator animator, float moveSpeed) {
		// Pause before animating
		yield return new WaitForSeconds (animateDelay);

		// Set the b position
		Vector3 b = newPos;

		// Move the text over since the anchor is in the center
		b.x -= 0;

		// Keep the Z the same so we don't move behind the sign
		b.z = a.z;

		// Bump it up just a tad
		b.y += 0.0f;

		// Simple loop to linearly move from point a to be
		for (float i = 0.0f; i < 1.0f; i += moveSpeed) {
			// Clamp the value in case the iterator changes by a larger amount
			if (i > 1.0f) {
				i = 1.0f;
			}

			// Lerp a derp
			text.transform.position = Vector3.Lerp (a, b, i);

			// Sllight pause so it doesn't happen instantly
			yield return new WaitForSeconds (0.01f);
		}

		// Degrow!
		DegrowObject (animator);
	}

	void DegrowObject(Animator animator) {
		// Call the cb
		callback(nextScore);

		// Get rid of the player text
		if (text != null) {
			Destroy (text);
		}
		gameObject.SetActive (false);

		// Begin degrow
		//		StartCoroutine(ScaleDown (animator));
	}

	IEnumerator ScaleDown(Animator animator) {
		animator.SetTrigger ("BeginDegrow");

		yield return new WaitUntil(() => isPlaying(animator, "Idle", 0));

		// Deactivate
		gameObject.SetActive (false);
	}

	// Return if the animator is back to idle
	bool isPlaying(Animator anim, string state, int layer) {
		return anim.GetCurrentAnimatorStateInfo (layer).IsName (state) && anim.GetCurrentAnimatorStateInfo (0).normalizedTime <= 1.0f;
	}
}
