﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Video;

public class ScoresServiceScript : MonoBehaviour {

	// Server location
	public string url = "http://192.168.162.124:5454/leaders";

	// How often (in seconds) to refetch the data
	public float refreshInterval = 2.0f;

	public Camera MainCam;
	public Camera CharityCam;

	// Keep track of the current leaderboard data
	// TODO: LOCK THIS DATA
	private LeaderboardEntryCollection currentData = new LeaderboardEntryCollection();

	// SceneManager Reference
	private SceneManager sceneManager;

	// Resolution variables
	private Vector2 newResolution = new Vector2 (1920f, 1080f);
	private Vector2 newPosition = new Vector2 (0f, 0f);

	public float fadeTime = 1.0f;
	bool inProgress = false;
	bool swap = false;

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    private static extern bool SetWindowPos(IntPtr hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);
    [DllImport("user32.dll", EntryPoint = "FindWindow")]
    public static extern IntPtr FindWindow(System.String className, System.String windowName);

    public static void SetPosition(int x, int y, int resX = 0, int resY = 0)
    {
        SetWindowPos(FindWindow(null, "HumiraLeaderboard"), 0, x, y, resX, resY, resX * resY == 0 ? 1 : 0);
    }
#endif

    private void Awake() {
        
    }

    IEnumerator SetPos(float x, float y)
    {
        yield return new WaitForSecondsRealtime(5f);
        SetPosition((int) x, (int) y);
    }

    // Use this for initialization
    void Start() {
		var amountOfCycles = 3;
		var position = "a";
		var VideoOnScreenTime = 112f;
        var charityOnScreenTime = 10f;
		string[] args = System.Environment.GetCommandLineArgs ();
		//args = mockCommandLine (); //Debug
		for (int i = 0; i < args.Length; i++) {
			//Debug.Log ("ARG " + i + ": " + args [i]);
			if (args [i] == "-url") {
				url = args [i + 1];
			}

			if (args [i] == "-width") {
				newResolution.x = float.Parse(args [i + 1]);
			}

			if (args [i] == "-height") {
				newResolution.y = float.Parse(args [i + 1]);
			}

			if (args [i] == "-left") {
				newPosition.x = float.Parse(args [i + 1]);
			}

			if (args [i] == "-top") {
				newPosition.y = float.Parse(args [i + 1]);
			}

			if (args [i] == "-cycles") {
				amountOfCycles = int.Parse(args [i + 1]);
			}

			if (args [i] == "-position") {
				position = args [i + 1];
			}

			if (args [i] == "-video") {
				VideoOnScreenTime = float.Parse(args [i + 1]);
			}
		}

        Screen.SetResolution((int) newResolution.x, (int) newResolution.y, false);
        StartCoroutine(SetPos(newPosition.x, newPosition.y));

        // New resolution, and new position
        //setUpCamera (newResolution, newPosition);

        // Init leaders list
        currentData.leaders = new List<LeaderboardEntry> ();

		// Get the scene manager
		sceneManager = GameObject.FindObjectOfType (typeof(SceneManager)) as SceneManager;

		// Get the sync script and kick it off
		var syncManager = GameObject.Find("SyncManager");
		var syncScript = syncManager.GetComponent<SyncScript> ();
		syncScript.Init (amountOfCycles, position, charityOnScreenTime ,VideoOnScreenTime, url);
	}

void setUpCamera(Vector2 newResolution, Vector2 newPosition) {
		// The base screen resolution
		Vector2 baseResolution = new Vector2 (1080f, 1920f);

		// Resolution factor
		float m_width = 1f / baseResolution.x;
		float m_height = 1f / baseResolution.y;

		// Modify the given resolution by the correct factors
		Vector2 modifiedResolution = new Vector2 (newResolution.x * m_width, newResolution.y * m_height);

		// Calculate position based from the top left of the screen
		Vector2 topLeft = new Vector2((newPosition.x / baseResolution.x), 1 - ((newResolution.y + newPosition.y) / baseResolution.y));

		// Get the camera and apply the new position and resolution to the rect
		MainCam.rect = new Rect (topLeft, modifiedResolution);
		CharityCam.rect = new Rect (topLeft, modifiedResolution);

		// Scale the canvas to the new resolution
		GameObject mainCanvas = GameObject.Find ("Canvas");
		Canvas canvasRef = mainCanvas.GetComponentInChildren <Canvas>();
		//canvasRef.scaleFactor = newResolution.y * m_height;

		GameObject charityCanvas = GameObject.Find ("CharityCanvas");
		Canvas charityCanvasRef = charityCanvas.GetComponentInChildren <Canvas>();
		charityCanvasRef.scaleFactor = newResolution.y * m_height;
	}

	//string[] mockCommandLine() {
//		return new string[] { "-url", "http://192.168.162.124:5454", "-width", "540", "-height", "960", "-left", "0", "-top", "0", "-cycles", "3", "-position", "a", "-charity", "10" };
		//return new string[] { "-url", "http://192.168.162.151:3000/", "-width", "768", "-height", "1344", "-left", "0", "-top", "0", "-cycles", "3", "-position", "a", "-charity", "10" };
//		return new string[] { "-url", "http://192.168.162.124:3000", "-width", "1344", "-height", "1344", "-left", "0", "-top", "0", "-cycles", "3", "-position", "a", "-charity", "10" };
	//}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown ("space")) {
//			StartCoroutine( DoFade() );
//		}
	}

	public IEnumerator DoFade(float time) {
		if (inProgress == false) {

			inProgress = true;

			swap = !swap;

			yield return StartCoroutine (ScreenWipe.use.CrossFadePro (swap ? MainCam : CharityCam, swap ? CharityCam : MainCam, time));

			inProgress = false;
		}
	}

	// Begin the polling interval
	public void beginPolling() {
		// After one second, start the requests
		InvokeRepeating ("getData", 1.0f, refreshInterval);
	}

	// Provide the current data in a list of LeaderboardEntry's
	public List<LeaderboardEntry> getScores() {
		return currentData.leaders;
	}

	// Stop the polling
	public void stop() {
		CancelInvoke ();
	}

	// Fetch the leaders
	void getData() {
		//New request
		WWW www = new WWW(url +"scores");

		//Async wait for data
		StartCoroutine (waitForData (www));
	}
		
	// Coroutine to wait for data
	IEnumerator waitForData(WWW www) {
		// Wait
		yield return www;

		// Error check
		if (www.error == null) {
			// Convert JSON data to a leaderboardCollection
			LeaderboardEntryCollection data = JsonUtility.FromJson<LeaderboardEntryCollection>("{\n  \"leaders\": " + www.text + "\n}");

			// Handle the new data
			handleNewData (data);
		} else {
			Debug.Log ("An error occurred while fetching the leaderboard data.");
		}
	}

	// Handle new data
	void handleNewData(LeaderboardEntryCollection leaderData) {
		// Compare the new and old data
		if (AreCollectionsEqual(leaderData, currentData) == false) {
			sceneManager.HandleNewScores (leaderData.leaders, currentData.leaders);
		}

		// Set the new data
		currentData = leaderData;
	}

	// Compare two lists of leaderboard entries
	bool AreCollectionsEqual(LeaderboardEntryCollection _new, LeaderboardEntryCollection _old) {
		if (_old == null) {
			return false;
		}

		if (_new.leaders.Count != _old.leaders.Count) {
			return false;
		}

		for (int i = 0; i < _new.leaders.Count; i++) {
			if ((_old.leaders[i].name != _new.leaders[i].name) || (_old.leaders[i].value != _new.leaders[i].value)) {
				return false;
			}
		}

		return true;
	}
}

[System.Serializable]
public class LeaderboardEntryCollection
{
	public List<LeaderboardEntry> leaders;
}