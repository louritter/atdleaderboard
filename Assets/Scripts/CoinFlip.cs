﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinFlip : MonoBehaviour {

	// Final size of the coin
	public int finalSize = 20;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Rigidbody> ().AddTorque (new Vector3(0, -20, 0));
		StartCoroutine (MoveAndScaleTo ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	// Move to the approapriate position in front of the camera
	IEnumerator MoveAndScaleTo() {
		// Position values
		Vector3 start = transform.position; // Current position
		Vector3 destination = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, Camera.main.nearClipPlane + (finalSize / 2))); // Center screen, directly in front of camera

		// Scale values
		Vector3 startScale = transform.localScale;
		Vector3 destinationScale = transform.localScale + new Vector3(finalSize, 0, finalSize);

		// Rotational values
		Quaternion destinationRotation = Camera.main.transform.rotation;

		// Loop control
		float journeyProgress = 0.0f;
		bool journeyComplete = false;
		int success = 0;

		// Loop until coin is in place
		while (!journeyComplete) {

			// Keep track of our success
			success = 2;

			// Apply position
			if (!(Vector3.Distance(transform.position, destination) <= 0.1f)) {
				transform.position = Vector3.Lerp (start, destination, journeyProgress);
				success--;
			}

			// Apply scale
			if (!(Vector3.Distance(transform.localScale, destinationScale) <= 2.0f)) {
				transform.localScale = Vector3.Lerp (startScale, destinationScale, journeyProgress);
				success--;
			}

			// Wait for success and rotation to line up
			if (success == 2) {
				if (AlmostEqual(transform.rotation.z, destinationRotation.z - 0.707f, 0.0001f)) {
					// End the loop by marking our journey as finished
					journeyComplete = true;

					// Stop all of our velocity
					gameObject.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
				}
			}

			// Increment the progress of the lerps
			journeyProgress += 0.01f;

			// Wait for a moment so that it creates a proper tween
			yield return new WaitForSeconds (0.01f);
		}
	}

	// Returns if two absolute values of floats are almost equal, given a certain allowance
	bool AlmostEqual(float val1, float val2, float allowance) {
		if ((Mathf.Abs(val1) >= Mathf.Abs(val2) - allowance) && (Mathf.Abs(val1) <= Mathf.Abs(val2) + allowance)) {
			return true;
		}
		return false;
	}
}
