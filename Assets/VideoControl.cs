﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoControl : MonoBehaviour {

	public GameObject videoGameObject;
	private VideoPlayer videoOverlay;
	private bool isPlaying = false;
	private bool isFading = false;

	// Use this for initialization
	void Start () {
		videoOverlay = videoGameObject.GetComponent<VideoPlayer> ();
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown ("v")) {
//			if (isPlaying) {
//				StartCoroutine (FadeVideo (false));
//			} else {
//				StartCoroutine (FadeVideo (true));
//			}
//		}
			
	}

	public IEnumerator FadeVideo(bool direction, float time) {
		float elapsedTime = 0;

		if (direction) {
			isPlaying = true;
			videoOverlay.time = 0;
			videoOverlay.Play ();
		} else {
			isPlaying = false;
			videoOverlay.Pause ();
		}

		while (elapsedTime < time) {
			if (direction) {
				videoOverlay.targetCameraAlpha = Mathf.Lerp (0f, 1f, (elapsedTime / time));
			} else {
				videoOverlay.targetCameraAlpha = Mathf.Lerp (1f, 0f, (elapsedTime / time));
			}

			elapsedTime += Time.deltaTime;

			yield return new WaitForFixedUpdate ();         // Leave the routine and return here in the next frame
		}
			
		videoOverlay.targetCameraAlpha = direction ? 1f : 0f;
	}

	public void ToggleVideo(bool direction) {
		if (direction) {
			videoOverlay.targetCameraAlpha = 1f;
			isPlaying = true;
			videoOverlay.time = 0;
			videoOverlay.Play ();
		} else {
			videoOverlay.targetCameraAlpha = 0f;
			isPlaying = false;
			videoOverlay.Pause ();
		}
	}

	public void VideoPlayerControl(bool playing) {
		isPlaying = playing;
		if (isPlaying) {
			videoOverlay.Play ();
		} else {
			videoOverlay.Pause ();
		}
	}
}
