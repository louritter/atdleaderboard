﻿using UnityEngine;
using System.Collections;

public class WaveBG : MonoBehaviour {

	public float speed = 2.0f;	
	private Vector3 direction;

	// Use this for initialization
	void Start () {
		Debug.Log("I am alive!");
		direction = Vector3.right;

	}

	// Update is called once per frame
	void Update () {
		var pos = transform.position.x;

		if (pos >= 12.1f) {
			direction = Vector3.left;
		} else if (pos <= -12.1f) {
			direction = Vector3.right;
		}
		transform.Translate (direction * Time.deltaTime * speed);

	
		
	}
	 

}