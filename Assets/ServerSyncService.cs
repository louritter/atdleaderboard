﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSyncService : MonoBehaviour {

	// Keep track of the meter
	string meterValue = "";

	// Keep track of the different times
	string oppositeLeaderboardTime = "";

	// Server url
	string url = "";

	// Which leaderboard are we
	string position = "";

	// Use this for initialization
	void Start () {
		
	}

	public void Init(string server_location, string leaderboard_position) {
		url = server_location;
		position = leaderboard_position;

		InvokeRepeating ("MakeRequests", 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void MakeRequests() {
		StartCoroutine (GetOppositeLeaderboardTimeFromServer ());
		StartCoroutine (GetMeterValueFromServer ());
	}

	IEnumerator GetOppositeLeaderboardTimeFromServer() {
		// Get the video end time for the opposite leaderboard
		WWW www = new WWW(url + "sync?id=" + position);

		// Wait for response
		yield return www;

		// Save it
		oppositeLeaderboardTime = www.text;
	}

	IEnumerator GetMeterValueFromServer() {
		// Get the video end time for the opposite leaderboard
		WWW www = new WWW(url + "meter");

		// Wait for response
		yield return www;

		// Save it
		meterValue = www.text;
	}

	public string GetMeterValue() {
		return meterValue;
	}

	public string GetOppositLeaderboardTime() {
		return oppositeLeaderboardTime;
	}
}
