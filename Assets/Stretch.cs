﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stretch : MonoBehaviour {
	// Transform to stretch (meter quad)
	public Transform targetTransform;

	// Number images
	public GameObject numberImage1;
	public GameObject numberImage2;
	public GameObject numberImage3;
	public GameObject numberImage4;
	public GameObject numberImage5;
	public GameObject numberImage6;
	public GameObject numberImageMax;

	// Image X holders
	private float imageTargetX;
	private float currentImageX;
	private float imageMaxTargetX;

	// Number image speed
	public float numberSpeed = 100f;

	// Keep track of which images animated
	private bool[] animatedNumbers = new bool[] { false, false, false, false, false, false };

	// Control variables
	public float animationSeconds = 2f;
	private float currentTime = 0f;
	bool isAnimating = false;

	// Bar values
	private float[] wholeMeterTicksPosition = new float[] { -275f, -232.3f, -192.2f, -152.4f, -112.3f, -71.9f, -58.3f };
	private float[] wholeMeterTicksScale = new float[] { 27.57f, 113.0284f, 193.1457f, 272.6694f, 352.7867f, 433.4973f, 460.7965f };

	// Set up point addociation
	private int[] pointToMeter = new int[] { 100, 200, 300, 400, 500, 600, 650 };

	// Initialize size control values
	private float targetScaleValue = 32.31761f;
	private float targetPositionValue = -272.5f;
	private float currentScaleValue = 32.31761f;
	private float currentPositionValue = -272.5f;

	// Use this for initialization
	void Start () {
		InitImages ();
	}
	
	// Update is called once per frame
	void Update () {
		HandleMeterAnimation ();
		HandleNumberAnimation ();

//		if (Input.GetKeyDown ("r")) {
//			Reset ();
//		}
//
//		if (Input.GetKeyDown ("3")) {
//			AnimateBar (350);
//		}
//
//		if (Input.GetKeyDown ("9")) {
//			AnimateBar (650);
//		}
	}

	public void Reset() {
		ResetImages ();
		targetScaleValue = 32.31761f;
		targetPositionValue = -272.5f;
		currentScaleValue = 32.31761f;
		currentPositionValue = -272.5f;
	}

	void InitImages() {
		imageTargetX = numberImage1.transform.localPosition.x;
		imageMaxTargetX = numberImageMax.transform.localPosition.x;
		currentImageX = imageTargetX + 200f;
		ResetImages ();
	}

	void ResetImages() {
		numberImage1.transform.localPosition = new Vector3 (currentImageX, numberImage1.transform.localPosition.y, numberImage1.transform.localPosition.z);
		numberImage2.transform.localPosition = new Vector3 (currentImageX, numberImage2.transform.localPosition.y, numberImage2.transform.localPosition.z);
		numberImage3.transform.localPosition = new Vector3 (currentImageX, numberImage3.transform.localPosition.y, numberImage3.transform.localPosition.z);
		numberImage4.transform.localPosition = new Vector3 (currentImageX, numberImage4.transform.localPosition.y, numberImage4.transform.localPosition.z);
		numberImage5.transform.localPosition = new Vector3 (currentImageX, numberImage5.transform.localPosition.y, numberImage5.transform.localPosition.z);
		numberImage6.transform.localPosition = new Vector3 (currentImageX, numberImage6.transform.localPosition.y, numberImage6.transform.localPosition.z);
		numberImageMax.transform.localPosition = new Vector3 (currentImageX + 50f, numberImageMax.transform.localPosition.y, numberImageMax.transform.localPosition.z);
		animatedNumbers = new bool[] { false, false, false, false, false, false, false };
	}

	// Animate the meter based on a point value
	public void AnimateBar(int points) {
		int diff = 0;
		int curr = 0;

		//use the top if it's not found
		curr = pointToMeter.Length - 1; 
		for (int i = 0; i < pointToMeter.Length; i++) {
			if(pointToMeter[i] >= points ) {
				curr = i;
				break;
			}
		}

		// Get the bounding point values
		float topPointVal = pointToMeter [curr];
		float bottomPointVal = curr - 1 > 0 ? pointToMeter [curr - 1] : 0f;

		// Get the bounding position values
		float topPosVal = wholeMeterTicksPosition [curr];
		float bottomPosVal = curr - 1 > 0 ? wholeMeterTicksPosition [curr - 1] : 0f;

		// Get the bounding scale values
		float topScaleVal = wholeMeterTicksScale [curr];
		float bottomScaleVal = curr - 1 > 0 ? wholeMeterTicksScale [curr - 1] : 0f;

		// Calculate units
		float unit = topPointVal - bottomPointVal;
		float punit = points - bottomPointVal;
		float sunit = topPosVal - bottomPosVal;
		float ssunit = topScaleVal - bottomScaleVal;

		// Calculate new position and scale
		float newPosition = (punit * sunit) / unit + bottomPosVal;
		float newScale = (punit * ssunit) / unit + bottomScaleVal;

		// Trigger animation
		BeginAnimation(newScale, newPosition);
	}

	// Begin animating the meter to the given scale and position
	void BeginAnimation(float newScale, float newPosition) {
		currentScaleValue = targetTransform.localScale.y;
		currentPositionValue = targetTransform.localPosition.y;

		targetScaleValue = newScale;
		targetPositionValue = newPosition;
	}

	void HandleMeterAnimation() {
		if (!Mathf.Approximately(targetTransform.localScale.y, targetScaleValue)) {
			isAnimating = true;

			currentTime += Time.deltaTime;

			targetTransform.localScale = new Vector3 (targetTransform.localScale.x, Mathf.Lerp (currentScaleValue, targetScaleValue, currentTime / animationSeconds), targetTransform.localScale.z);
			targetTransform.localPosition = new Vector3 (targetTransform.localPosition.x, Mathf.Lerp (currentPositionValue, targetPositionValue, currentTime / animationSeconds), targetTransform.localPosition.z);
		} else {
			isAnimating = false;
			currentTime = 0f;
		}
	}

	void HandleNumberAnimation() {
		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [0]) {
			if (animatedNumbers [0] == false) {
				animatedNumbers [0] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage1.transform,
						new Vector3 (currentImageX, numberImage1.transform.localPosition.y, numberImage1.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage1.transform.localPosition.y, numberImage1.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [1]) {
			if (animatedNumbers [1] == false) {
				animatedNumbers [1] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage2.transform,
						new Vector3 (currentImageX, numberImage2.transform.localPosition.y, numberImage2.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage2.transform.localPosition.y, numberImage2.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [2]) {
			if (animatedNumbers [2] == false) {
				animatedNumbers [2] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage3.transform,
						new Vector3 (currentImageX, numberImage3.transform.localPosition.y, numberImage3.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage3.transform.localPosition.y, numberImage3.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [3]) {
			if (animatedNumbers [3] == false) {
				animatedNumbers [3] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage4.transform,
						new Vector3 (currentImageX, numberImage4.transform.localPosition.y, numberImage4.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage4.transform.localPosition.y, numberImage4.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [4]) {
			if (animatedNumbers [4] == false) {
				animatedNumbers [4] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage5.transform,
						new Vector3 (currentImageX, numberImage5.transform.localPosition.y, numberImage5.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage5.transform.localPosition.y, numberImage5.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [5]) {
			if (animatedNumbers [5] == false) {
				animatedNumbers [5] = true;
				StartCoroutine(
					MoveToQuart (
						numberImage6.transform,
						new Vector3 (currentImageX, numberImage6.transform.localPosition.y, numberImage6.transform.localPosition.z),
						new Vector3 (imageTargetX, numberImage6.transform.localPosition.y, numberImage6.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}

		if (targetTransform.localPosition.y >= wholeMeterTicksPosition [6]) {
			if (animatedNumbers [6] == false) {
				animatedNumbers [6] = true;
				StartCoroutine(
					MoveToQuart (
						numberImageMax.transform,
						new Vector3 (currentImageX, numberImageMax.transform.localPosition.y, numberImageMax.transform.localPosition.z),
						new Vector3 (imageMaxTargetX, numberImageMax.transform.localPosition.y, numberImageMax.transform.localPosition.z),
						numberSpeed
					)
				);
			}
		}
	}
		
	IEnumerator MoveToQuart(Transform objectToMove, Vector3 a, Vector3 b, float speed) {
		float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
		float t = 0;
		while (t <= 1.0f) {
			t += step; // Goes from 0 to 1, incrementing by step each time
			//objectToMove.localPosition = Vector3.Lerp(a, b, t); // Move objectToMove closer to b

			objectToMove.localPosition = new Vector3 (
				EaseOutQuart(t, a.x, b.x - a.x, 1f),
				objectToMove.localPosition.y,
				objectToMove.localPosition.z
			);

			yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
		}
		objectToMove.localPosition = b;
	}

	float EaseOutQuart(float t, float b, float c, float d) {
		t /= d;
		t--;
		return -c * (t*t*t*t - 1) + b;
	}
}
