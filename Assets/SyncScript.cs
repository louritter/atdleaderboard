﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncScript : MonoBehaviour {

	// Which leaderboard identifier am I
	private string position = "a";

	// How long should the charity show on screen
	private float charityOnScreenTime = 10f;
    private float VideoOnScreenTime = 112f;
	// Server location
	private string url = "";

	// How long the video is (in seconds)
	public float videoLength = 112f;

	// How long should the fade transitions be
	public float videoFadeTransition = 1f;

	// Keep track of the meter value
	private int meterValue = 0;

	// Should we transition to charity meter
	private bool shouldGoToCharity = false;

	// For post-charity state change
	private States postCharityState = States.NONE;

	// Get reference objects through inspector
	public GameObject videoOverlay;
	public GameObject scoresService;
	public GameObject quad;

	// Set up references
	private VideoControl videoScript;
	private ScoresServiceScript scoreScript;
	private Stretch stretchScript;
	private ServerSyncService serverSyncService;

	// Have we triggered our states?
	private bool triggeredHandleStartingVideoFromLeaderboard = false;
	private bool triggeredHandleStartingVideoFromCharity = false;
	private bool triggeredHandleShowingVideo = false;
	private bool triggeredHandleStoppingVideoGoingToCharity = false;
	private bool triggeredHandleStoppingVideoGoingToLeaderboard = false;
	private bool triggeredHandleShowingLeaderboard = false;
	private bool triggeredHandleGoingToCharityFromLeaderboard = false;
	private bool triggeredHandleShowingCharity = false;
	private bool triggeredHandleStartingLeaderboardFromCharity = false;

	// Setup states
	enum States {
		NONE,
		STARTING_VIDEO_FROM_LEADERBOARD,
		STARTING_VIDEO_FROM_CHARITY,
		SHOWING_VIDEO,
		STOPPING_VIDEO_GOING_TO_CHARITY,
		STOPPING_VIDEO_GOING_TO_LEADERBOARD,
		STARTING_LEADERBOARD_FROM_CHARITY,
		SHOWING_LEADERBOARD,
		GOING_TO_CHARITY_FROM_LEADERBOARD,
		SHOWING_CHARITY
	}

	// We don't have a starting state until we are ready
	private States state = States.NONE;

	// Use this for initialization
	void Start () {
		videoScript = videoOverlay.GetComponent<VideoControl> ();
		scoreScript = scoresService.GetComponent<ScoresServiceScript> ();
		stretchScript = quad.GetComponent<Stretch> ();
	}

	// Init the sync script with proper variables
	public void Init(int cycles, string pos, float charity,float video,string serverLocation) {
		// Set the current leaderboard id, url, and charity on screen time
		position = pos;
		charityOnScreenTime = charity;
		url = serverLocation;
        VideoOnScreenTime = video;
		// Tell the server sync service to start polling the server for information
		serverSyncService = gameObject.GetComponent<ServerSyncService> ();
		serverSyncService.Init(url, pos);

		// Trigger the start state of the state machine
		state = States.STARTING_VIDEO_FROM_LEADERBOARD;
	}
	
	// Update is called once per frame
	void Update () {
		HandleState (state);
	}

	// Handle the current state
	void HandleState(States state) {
		switch (state) {
		case States.STARTING_VIDEO_FROM_LEADERBOARD:
			StartCoroutine (handleStartingVideoFromLeaderboard ());
			break;
		case States.STARTING_VIDEO_FROM_CHARITY:
			StartCoroutine (handleStartingVideoFromCharity ());
			break;
		case States.SHOWING_VIDEO:
			StartCoroutine (handleShowingVideo ());
			break;
		case States.STOPPING_VIDEO_GOING_TO_CHARITY:
			StartCoroutine (handleStoppingVideoGoingToCharity ());
			break;
		case States.STOPPING_VIDEO_GOING_TO_LEADERBOARD:
			StartCoroutine (handleStoppingVideoGoingToLeaderboard ());
			break;
		case States.SHOWING_LEADERBOARD:
			StartCoroutine (handleShowingLeaderboard ());
			break;
		case States.GOING_TO_CHARITY_FROM_LEADERBOARD:
			StartCoroutine (handleGoingToCharityFromLeaderboard ());
			break;
		case States.SHOWING_CHARITY:
			StartCoroutine (handleShowingCharity ());
			break;
		case States.STARTING_LEADERBOARD_FROM_CHARITY:
			StartCoroutine (handleStartingLeaderboardFromCharity ());
			break;
		}
	}

	IEnumerator handleStartingVideoFromLeaderboard() {
		if (!triggeredHandleStartingVideoFromLeaderboard) {
			// We have triggered
			triggeredHandleStartingVideoFromLeaderboard = true;

			// Fade video in
			StartCoroutine (videoScript.FadeVideo (true, videoFadeTransition));

			// Post the video length
			StartCoroutine (postVideoStatus ());

			// Wait for video to fade in completely before moving on to the next state
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Reset triggered status
			triggeredHandleStartingVideoFromLeaderboard = false;

			// Update the state
			state = States.SHOWING_VIDEO;
		}
	}

	IEnumerator handleStartingVideoFromCharity() {
		if (!triggeredHandleStartingVideoFromCharity) {
			// We have triggered
			triggeredHandleStartingVideoFromCharity = true;

			// Immediately show video
			videoScript.ToggleVideo (true);

			// Post the video length
			StartCoroutine (postVideoStatus ());

			// Fade the camera
			StartCoroutine (scoreScript.DoFade (videoFadeTransition));

			// Wait for camera to fade in completely before moving on to the next state
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Reset the meter
			stretchScript.Reset ();

			// Reset triggered status
			triggeredHandleStartingVideoFromCharity = false;

			// Update the state
			state = States.SHOWING_VIDEO;
		}
	}

	IEnumerator handleShowingVideo() {
		if (!triggeredHandleShowingVideo) {
			// We have triggered
			triggeredHandleShowingVideo = true;

			// Get whether or not we should transition (in 5 seconds)

			// Wait for video to finish
			yield return new WaitForSecondsRealtime (VideoOnScreenTime - videoFadeTransition);

			// Reset triggered status
			triggeredHandleShowingVideo = false;

			// Update the state
			if (shouldGoToCharity) {
				postCharityState = States.STARTING_LEADERBOARD_FROM_CHARITY;
				state = States.STOPPING_VIDEO_GOING_TO_CHARITY;
			} else {
				state = States.STOPPING_VIDEO_GOING_TO_LEADERBOARD;
			}
		}
	}

	IEnumerator handleStoppingVideoGoingToCharity() {
		if (!triggeredHandleStoppingVideoGoingToCharity) {
			// We have triggered
			triggeredHandleStoppingVideoGoingToCharity = true;

			// Pause the video (but don't hide it)
			videoScript.VideoPlayerControl (false);

			// Reset the meter
			stretchScript.Reset ();

			// Fade the camera
			StartCoroutine (scoreScript.DoFade (videoFadeTransition));

			// Get the meter value
			string stringMeterValue = serverSyncService.GetMeterValue();

			// Parse the meter value
			if (stringMeterValue != "") {
				meterValue = int.Parse (stringMeterValue);
			}

			// Animate the bar
			stretchScript.AnimateBar (meterValue);

			// Wait for camera to fade in completely before moving on to the next state
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Hide the video player (now that we can't see it)
			videoScript.ToggleVideo (false);

			// Reset triggered status
			triggeredHandleStoppingVideoGoingToCharity = false;

			// Update the state
			state = States.SHOWING_CHARITY;
		}
	}

	IEnumerator handleStoppingVideoGoingToLeaderboard() {
		if (!triggeredHandleStoppingVideoGoingToLeaderboard) {
			// We have triggered
			triggeredHandleStoppingVideoGoingToLeaderboard = true;

			// Fade out the video
			StartCoroutine (videoScript.FadeVideo(false, videoFadeTransition));

			// Wait for video to completely fade out
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Reset triggered status
			triggeredHandleStoppingVideoGoingToLeaderboard = false;

			// Update the state
			state = States.SHOWING_LEADERBOARD;
		}
	}

	IEnumerator handleShowingLeaderboard() {
		if (!triggeredHandleShowingLeaderboard) {
			// We have triggered
			triggeredHandleShowingLeaderboard = true;

			// Get the opposite leaderboard time
			string oppositeTime = serverSyncService.GetOppositLeaderboardTime ();

			// If the time string is valid
			if ((oppositeTime != "") && (oppositeTime != "0")) {
				// Parse the response text
				System.DateTime d = System.DateTime.Parse (oppositeTime);

				// Get the time difference
				System.TimeSpan t = d - System.DateTime.Now;

				// Get time length in seconds
				var transitionIn = t.TotalMilliseconds / 1000;

				// If the target time is in the future
				if (transitionIn > 0) {
					// Get whether or not we should transition (in 5 seconds)
					// Wait for that time
					yield return new WaitForSecondsRealtime ((float) transitionIn);

					// Update the state
					if (shouldGoToCharity) {
						postCharityState = States.STARTING_VIDEO_FROM_CHARITY;
						state = States.GOING_TO_CHARITY_FROM_LEADERBOARD;
					} else {
						state = States.STARTING_VIDEO_FROM_LEADERBOARD;
					}
				}
			}

			// Reset triggered status
			triggeredHandleShowingLeaderboard = false;
		}
	}

	IEnumerator handleGoingToCharityFromLeaderboard() {
		if (!triggeredHandleGoingToCharityFromLeaderboard) {
			// We have triggered
			triggeredHandleGoingToCharityFromLeaderboard = true;

			// Reset the meter
			stretchScript.Reset ();

			// Fade the camera
			StartCoroutine (scoreScript.DoFade (videoFadeTransition));

			// Get the meter value
			string stringMeterValue = serverSyncService.GetMeterValue();

			// Parse the meter value
			if (stringMeterValue != "") {
				meterValue = int.Parse (stringMeterValue);
			}

			// Animate the bar
			stretchScript.AnimateBar (meterValue);

			// Wait for the camera to completely fade
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Reset triggered status
			triggeredHandleGoingToCharityFromLeaderboard = false;

			// Update the state
			state = States.SHOWING_CHARITY;
		}
	}

	IEnumerator handleShowingCharity() {
		if (!triggeredHandleShowingCharity) {
			// We have triggered
			triggeredHandleShowingCharity = true;

			// Wait for the correct amount of on screen time for the charity
			yield return new WaitForSecondsRealtime (charityOnScreenTime - videoFadeTransition);

			// Reset triggered status
			triggeredHandleShowingCharity = false;

			// Update state
			state = postCharityState;
		}
	}

	IEnumerator handleStartingLeaderboardFromCharity() {
		if (!triggeredHandleStartingLeaderboardFromCharity) {
			// We have triggered
			triggeredHandleStartingLeaderboardFromCharity = true;

			// Fade the camera
			StartCoroutine (scoreScript.DoFade (videoFadeTransition));

			// Wait for the camera to completely fade
			yield return new WaitForSecondsRealtime (videoFadeTransition);

			// Reset the meter
			stretchScript.Reset ();

			// Reset triggered status
			triggeredHandleStartingLeaderboardFromCharity = false;

			// Update state
			state = States.SHOWING_LEADERBOARD;
		}
	}

	IEnumerator postVideoStatus() {
		// Get the current date
		System.DateTime d = System.DateTime.Now;

		// Add the video duration to the date
		d = d.AddSeconds ((double) VideoOnScreenTime);
        Debug.Log(VideoOnScreenTime);
		// Set up a new post with the datetime and id
		WWWForm form = new WWWForm();
		form.AddField( "video", d.ToString() );
		form.AddField( "id", position );

		// Make the new request
		WWW www = new WWW(url + "sync", form);

		// Wait for response
		yield return www;
	}

	/**
	 * waitTime - How long to wait
	 */
	IEnumerator getShouldTransitionToCharity(float waitTime) {
		yield return new WaitForSecondsRealtime (waitTime);

		// Should we transition?
		WWW www = new WWW(url + "transition?id=" + position);

		// Wait for response
		yield return www;

		// Parse if possible
		if (www.text != "") {
			shouldGoToCharity = false;
		}
	}
}
