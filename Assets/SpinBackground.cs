﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		RotateRight ();
	}


	void RotateRight(){
		transform.Rotate(Vector3.back * (Time.deltaTime * 5), Space.World);

	}
}
